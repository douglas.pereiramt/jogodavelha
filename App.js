import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';

export default function App() {

  const [screen, setScreen] = useState('intro');
  const [player, setPlayer] = useState('');
  const [gameBoard, setGameBoard] = useState([]);
  const [winner, setWinner] = useState(''); 
  const [moves, setMoves] = useState(0);


  switch (screen) {
    case 'intro':
      return introPage();
    case 'menu':
      return menu();
    case 'game':
      return game();
    case 'winner':
      return winnerPage();
  }

  function menu() {
    return (
      <View style={styles.container}>
        <StatusBar style="auto" />
        <Text style={{ color: '#fff', fontSize: 30 }}>Selecione X ou O para começar</Text>
        <View style={styles.items}>
          <TouchableOpacity style={styles.players} onPress={() => begin('X')}>
            <Text style={styles.playerX}>X</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.players} onPress={() => begin('O')}>
            <Text style={styles.playerO}>O</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  function introPage() {
    return (
      <View style={styles.container}>
        <StatusBar style="auto" />
        <Text style={styles.title}>#Bem vindo ao nosso Jogo da Velha#</Text>
        <Button title="Iniciar" color="#ec4dbc" onPress={() => setScreen('menu')} ></Button>
      </View>
    );
  }

  function begin(player) {
    setPlayer(player);

    setMoves(9);

    setGameBoard([
      ['', '', ''],
      ['', '', ''],
      ['', '', ''],
    ]);

    setScreen('game');
  }

  function play(lineNumber, columnNumber) {
    gameBoard[lineNumber][columnNumber] = player;
    setGameBoard([...gameBoard]);

    setPlayer(player === 'X' ? 'O' : 'X');
    checkWinner(gameBoard, lineNumber, columnNumber);
  }

  function checkWinner(gameBoard, lineNumber, columnNumber) {
    if(gameBoard[lineNumber][0] !== '' && gameBoard[lineNumber][0] === gameBoard[lineNumber][1] && gameBoard[lineNumber][1] === gameBoard[lineNumber][2]) {
      return endGame(gameBoard[lineNumber][0]);
    }

    if(gameBoard[0][columnNumber] !== '' && gameBoard[0][columnNumber] === gameBoard[1][columnNumber] && gameBoard[1][columnNumber] === gameBoard[2][columnNumber]) {
      return endGame(gameBoard[0][columnNumber]);
    }

    if(gameBoard[0][0] !== '' && gameBoard[0][0] === gameBoard[1][1] && gameBoard[1][1] === gameBoard[2][2]) {
      return endGame(gameBoard[0][0]);
    }

    if(gameBoard[0][2] !== '' && gameBoard[0][2] === gameBoard[1][1] && gameBoard[1][1] === gameBoard[2][0]) {
      return endGame(gameBoard[0][2]);
    }

    if(moves - 1 ===0) {
      return endGame('');
    }

    setMoves(moves - 1);
  }

  function endGame(player) {
    setWinner(player);
    setScreen('winner');
  }

  function winnerPage() {
    return (
      <View style={styles.container}>
        <StatusBar style="auto"/>
        {
          winner == '' && <Text style={{color: '#ec4dbc', fontSize: 30, margin:15}}>Deu Velha!!</Text>
        }

        {
          winner !== '' && 
          <>
            <Text style={styles.winner}>Vencedor</Text>

            <View style={styles.players}>
                <Text style={winner === 'X' ? styles.playerX : styles.playerO}>{winner}</Text>
              </View>
          </>
        }
        <Button onPress={() => setScreen('menu')} title="Jogar de Novo" color="#0bd18a"/>
      </View>
    )
  }

  function game() {
    return (
      <View style={styles.container}>
        <StatusBar style="auto"></StatusBar>
        {
          gameBoard.map((line, lineNumber) => {
            return (
              <View key={lineNumber} style={styles.items}>
                {
                  line.map((column, columnNumber) => {
                    return (
                      <TouchableOpacity key={columnNumber} style={styles.gamerBoard} disabled={column !== ''} onPress={() => play(lineNumber, columnNumber)}>
                        <Text style={column === 'X' ? styles.playerX : styles.playerO}>{column}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
            )
          })
        }

      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  }, title: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#0bd18a',
    margin: 15
  },
  items: {
    flexDirection: 'row',
  },
  players: {
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  playerX: {
    fontSize: 40,
    color: '#0bd18a'
  },
  playerO: {
    fontSize: 40,
    color: '#ec4dbc'
  },
  gamerBoard: {
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    backgroundColor: "#4c4646"
  },
  winner: {
    fontSize: 30,
    color: '#0bd18a'
  }
});
